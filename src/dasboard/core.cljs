(ns dasboard.core
  (:require-macros
   [cljs.core.async.macros :refer [go go-loop]])
  (:require
   [cljs.core.async :as async :refer [<! >! take! put! chan]]
   [huon.log :as log :refer [debug info warn error]]
   [otarta.core :as mqtt]
   [otarta.util :refer-macros [<err->]]
   [reagent.core :as r]))



(goog-define broker-url "ws://localhost:9001")

(defonce client (mqtt/client dasboard.core/broker-url
                             {#_#_:client-id  "ci-dashboard"
                              :keep-alive 10
                              :default-root-topic "gl-build-data"}))

;;;;
;;;; DB

(defonce db (r/atom {:subscriptions  {}
                     ;; sample build: {:id 12345 :project "eval/otarta" :branch "master" :status :success}
                     :master-builds  {}   ;; builds by project
                     :all-builds {}       ;; builds by id
}))
(def subscriptions (r/cursor db [:subscriptions]))
(def master-builds (r/cursor db [:master-builds]))
(def all-builds (r/cursor db [:all-builds]))


(def classes-by-build-status {:pending "is-warning"
                              :running "is-info"
                              :failed  "is-danger"
                              :success "is-success"})


(defn handle-builds [{:keys [master-only?]} sub]
  (info :handle-builds {:master-only? master-only?})
  (go-loop []
    (when-let [{payload :payload topic :topic} (<! sub)]
      (let [{{:strs [id ref status]}                      "object_attributes"
             {:strs [default_branch path_with_namespace]} "project"} payload
            master-branch? (= ref "master")
            build          {:id id :project path_with_namespace
                            :branch ref :status (keyword status)}]
        (info :handle-builds :received-msg {:master-only? master-only? :ref ref})
        (if (and master-only? master-branch?)
          (swap! master-builds assoc (:project build) build)
          (swap! all-builds assoc (:id build) build))
        (recur)))))


(defn ensure-subscription! [client topic-filter id handler]
  (go
    (info :ensure-subscription! {:topic-filter topic-filter :id id})
    (when-not (get @subscriptions id)
      (let [[err {sub-ch :ch}] (<! (mqtt/subscribe client topic-filter {:format :json}))]
        (if err
          (error "Subscribing failed:" err)
          (do (swap! subscriptions assoc id sub-ch)
              (handler sub-ch)))))))

;;;;
;;;; UI

(defn master-builds-list []
  [:div.tile.is-ancestor
   (for [[name {status :status}] (sort @master-builds)
         :let [status-class (get classes-by-build-status status)]]
     ^{:key name}
     [:div.tile.is-parent
      [:div.tile.is-child.notification {:class status-class}
       [:h1.is-size-1-desktop.has-text-weight-bold name]]])])


(defn all-builds-list []
  [:div.tile.is-ancestor
   (for [[id {:keys [project branch status]}] (->> @all-builds sort reverse (take 10))
         :let [status-class (get classes-by-build-status status)
               full-name (str project ":" branch)
               build-url (str "https://gitlab.com/" project "/pipelines/" id)]]
     ^{:key id}
     [:div.tile.is-parent
      [:div.tile.is-child.notification {:class status-class}
       [:h1.is-size-1-desktop.has-text-weight-bold [:a {:href build-url} full-name]]]])])


(defn projects-component []
  [:section.section
   [:div.container.is-fluid
    [:h1.title.has-text-light "Das Board"]
    [:h2.title.has-text-light "Master builds"]
    [master-builds-list]
    [:h2.title.has-text-light "All builds"]
    [all-builds-list]]])


(defn render []
  (r/render [projects-component]
            (.getElementById js/document "app")))


(defn init-logging! []
  (log/enable!)

  (log/set-root-level! :error)
  (log/set-level! "dasboard.core" :debug)
  (log/set-level! "otarta.core" :debug))


(defn init! []
  (info :init! {:client-config (:config client)})
  (init-logging!)
  (go
    (<! (ensure-subscription! client "+/+/refs/master" :master-builds
                              (partial handle-builds {:master-only? true})))
    (<! (ensure-subscription! client "+/+/refs/+" :all-builds
                              (partial handle-builds {:master-only? false}))))
  (render))

(init!)
